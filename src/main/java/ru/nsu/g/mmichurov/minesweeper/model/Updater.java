package ru.nsu.g.mmichurov.minesweeper.model;

public interface Updater {
    enum UPDATE_TYPE {
        VISUALS,
        TIMED,
        ALL
    }

    void update(UPDATE_TYPE type);
}
