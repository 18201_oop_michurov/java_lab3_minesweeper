package ru.nsu.g.mmichurov.minesweeper.view.cli;

import ru.nsu.g.mmichurov.minesweeper.model.GameBoard;
import ru.nsu.g.mmichurov.minesweeper.model.ScoreBoard;

import java.util.Scanner;

public class MainMenuView implements Runnable {
    private final Scanner scanner = new Scanner(System.in);

    private void runMenuInteraction() {
        var availableCommands = "Enter\n" +
                "\t\"new\" to start a new game\n" +
                "\t\"scores\" to view scoreboard\n" +
                "\t\"about\" to view game about info\n" +
                "\t\"exit\" to exit";
        System.out.println(availableCommands);

        var scanner = new Scanner(System.in);

        while (true) {
            System.out.print("> ");

            if (!scanner.hasNextLine()) {
                break;
            }

            var input = scanner.nextLine().strip();

            if ("new".equals(input)) {
                this.startNewGame();
            } else if ("scores".equals(input)) {
                this.printScores();
            } else if ("about".equals(input)) {
                this.printAbout();
            } else if ("exit".equals(input)) {
                return;
            } else if (!input.isBlank()) {
                System.out.println("Bad input");
            }
        }
    }

    private void printAbout() {
        var text = "Minesweeper is a single-player puzzle video game.\n" +
                "The objective of the game is to clear a rectangular\n" +
                "board containing hidden \"mines\" or bombs without\n" +
                "detonating any of them, with help from clues about\n" +
                "the number of neighboring mines in each field.";
        System.out.println(text);
    }

    private void printScores() {
        System.out.println("Player Name\t\tBoard Size\t\tMines Count\t\tTime");

        var scores = ScoreBoard.getScoresList();

        for (var e : scores) {
            System.out.printf(
                    "%s\t\t\t%d x %d\t\t\t%d\t\t\t\t%d\n",
                    e.playerName,
                    e.rowsCount,
                    e.columnsCount,
                    e.minesCount,
                    e.timeTaken
            );
        }
    }

    private String getPlayerName() {
        System.out.print("Player Name: ");
        if (scanner.hasNextLine()) {
            return scanner.nextLine();
        }

        return null;
    }

    private int getRowsCount() {
        System.out.print("Rows Count: ");
        if (scanner.hasNextLine()) {
            int rowsCount = Integer.parseInt(scanner.nextLine());
            if (rowsCount < 4 || rowsCount > 30) {
                System.out.printf("Rows Count value %d is out of allowed range [%d, %d]\n", rowsCount, 4, 30);
                return -1;
            }
            return rowsCount;
        }

        return -1;
    }

    private int getColumnsCount() {
        System.out.print("Columns Count: ");
        if (scanner.hasNextLine()) {
            int columnsCount = Integer.parseInt(scanner.nextLine());
            if (columnsCount < 4 || columnsCount > 30) {
                System.out.printf("Columns Count value %d is out of allowed range [%d, %d]\n", columnsCount, 4, 30);
                return -1;
            }
            return columnsCount;
        }

        return -1;
    }

    private int getMinesCount(int rowsCount, int columnsCount) {
        System.out.print("Mines Count: ");
        if (scanner.hasNextLine()) {
            int minesCount = Integer.parseInt(scanner.nextLine());
            if (minesCount < 4 || minesCount > 30) {
                System.out.printf(
                        "Mines Count value %d is out of allowed range [%d, %d]\n",
                        minesCount, 1, rowsCount * columnsCount
                );
                return -1;
            }
            return minesCount;
        }

        return -1;
    }

    private void startNewGame() {
        String playerName = this.getPlayerName();
        int rowsCount;
        int columnsCount;
        int minesCount;

        try {
            if ((rowsCount = this.getRowsCount()) == -1) {
                return;
            }
            if ((columnsCount = this.getColumnsCount()) == -1) {
                return;
            }
            if ((minesCount = this.getMinesCount(rowsCount, columnsCount)) == -1) {
                return;
            }

            var gameBoard = new GameBoard(playerName, rowsCount, columnsCount, minesCount);
            var gameRepr = new GameView(gameBoard);
            gameRepr.run();
        } catch (NumberFormatException e) {
            System.out.println("Bad Input Format");
        }
    }

    @Override
    public void run() {
        this.runMenuInteraction();
    }
}
