package ru.nsu.g.mmichurov.minesweeper;

import ru.nsu.g.mmichurov.minesweeper.view.gui.MainMenuWindow;

import javax.swing.*;

public class GUIApplication {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new MainMenuWindow());
    }
}
