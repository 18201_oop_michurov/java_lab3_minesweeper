package ru.nsu.g.mmichurov.minesweeper.view.cli;

import ru.nsu.g.mmichurov.minesweeper.model.GameBoard;
import ru.nsu.g.mmichurov.minesweeper.model.GameBoard.CELL_STATE;
import ru.nsu.g.mmichurov.minesweeper.model.ScoreBoard;
import ru.nsu.g.mmichurov.minesweeper.model.Updater;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.regex.Pattern;

public class GameView implements Updater, Runnable {
    private final GameBoard gameBoard;
    private final Scanner scanner = new Scanner(System.in);
    private final Pattern pattern = Pattern.compile("(\\d+)\\s+(\\d+)\\s*([fF])?");

    private boolean isUpdated;

    private final int rowsCount;
    private final int columnsCount;

    public GameView(GameBoard gameBoard) {
        this.gameBoard = gameBoard;
        this.rowsCount = this.gameBoard.getRowsCount();
        this.columnsCount = this.gameBoard.getColumnsCount();
        this.gameBoard.setView(this);
    }

    @Override
    public void update(UPDATE_TYPE type) {
        switch (type) {
            case ALL:
            case VISUALS:
                this.printBoard();
                System.out.println(this.gameBoard.getStatus());
                System.out.println(
                        LocalTime.ofSecondOfDay(
                                this.gameBoard.getElapsedSeconds()).format(DateTimeFormatter.ofPattern("HH:mm:ss")
                        )
                );
                this.isUpdated = true;
                break;
            case TIMED:
                break;
        }
    }

    private void printBoard() {
        System.out.print("    ");
        for (int i = 0; i < this.columnsCount; ++i) {
            System.out.printf("%d%s", i, " ".repeat(3 - String.valueOf(i).length()));
        }
        System.out.println();

        for (int y = 0; y < this.rowsCount; ++y) {
            System.out.printf("%d%s", y, " ".repeat(4 - String.valueOf(y).length()));

            for (int x = 0; x < this.columnsCount; ++x) {
                int cellValue = this.gameBoard.getCellValue(x, y);
                char c = (char) ('0' + cellValue);

                if (this.gameBoard.isInGame()) {
                    if (CELL_STATE.isMarked(cellValue)) {
                        c = 'F';
                    } else if (CELL_STATE.isCovered(cellValue)) {
                        c = '.';
                    } else {
                        c = c == '0' ? '_' : c;
                    }
                } else {
                    if (CELL_STATE.isMine(cellValue) && !CELL_STATE.isMarked(cellValue)) {
                        c = '*';
                    } else if (CELL_STATE.isMine(cellValue) && CELL_STATE.isMarked(cellValue)) {
                        c = 'F';
                    } else if (!CELL_STATE.isMine(cellValue) && CELL_STATE.isMarked(cellValue)) {
                        c = 'X';
                    } else if (CELL_STATE.isCovered(cellValue)) {
                        c = '.';
                    } else {
                        c = c == '0' ? '_' : c;
                    }
                }

                System.out.printf("%c  ", c);
            }
            System.out.println();
        }
    }

    private boolean shouldStartNewGame() {
        System.out.println(
                "Enter\n" +
                        "\t\"new\" to generate another board and play again\n" +
                        "\t\"exit\" to return to main menu"
        );

        while (true) {
            System.out.print(">> ");

            if (!this.scanner.hasNextLine()) {
                return false;
            }

            var line = this.scanner.nextLine().strip();

            if (line.isBlank()) {
                continue;
            }
            if ("exit".equals(line)) {
                return false;
            } else if ("new".equals(line)) {
                return true;
            } else {
                System.out.println("Bad format");
            }
        }
    }

    private void runGame() {
        this.gameBoard.newGame();

        while (true) {
            System.out.print(">> ");

            if (!this.scanner.hasNextLine()) {
                break;
            }
            var line = this.scanner.nextLine().strip();
            if (line.isBlank()) {
                continue;
            }
            if ("exit".equals(line)) {
                return;
            }
            var matcher = pattern.matcher(line);
            if (!matcher.matches()) {
                System.out.println("Bad format");
                continue;
            }

            this.isUpdated = false;
            this.gameBoard.updateCell(
                    Integer.parseInt(matcher.group(1)),
                    Integer.parseInt(matcher.group(2)),
                    matcher.group(3) == null
            );
            if (!this.isUpdated) {
                this.update(UPDATE_TYPE.VISUALS);
            }
            if (!this.gameBoard.isInGame()) {
                break;
            }
        }
    }

    @Override
    public void run() {
        do {
            System.out.println(
                    "Enter\n" +
                            "\tposition as {column} {row} [f|F]\n" +
                            "\t\"exit\" to return to main menu");

            this.runGame();
        } while (this.shouldStartNewGame());
    }
}
