package ru.nsu.g.mmichurov.minesweeper.view.gui;

import ru.nsu.g.mmichurov.minesweeper.model.GameBoard;
import ru.nsu.g.mmichurov.minesweeper.model.GameBoard.CELL_STATE;
import ru.nsu.g.mmichurov.minesweeper.model.Updater;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class GameWindow extends JFrame implements Updater, Runnable {
    GameBoard gameBoard;

    private final GameBoardPanel gameBoardPanel;
    private final InfoPanel infoPanel;


    public GameWindow(GameBoard gameBoard) throws IOException {
        super("Minesweeper");

        this.gameBoard = gameBoard;
        this.gameBoard.setView(this);
        this.gameBoardPanel = new GameBoardPanel();
        this.infoPanel = new InfoPanel();

        this.setResizable(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                SwingUtilities.invokeLater(new MainMenuWindow());
            }
        });

        this.addComponentsToPane(this.getContentPane());
        this.pack();
        this.setLocationRelativeTo(null);
        this.setMinimumSize(this.getSize());
    }

    private void addComponentsToPane(final Container pane) {
        pane.setLayout(new BorderLayout());

        pane.add(this.gameBoardPanel, BorderLayout.CENTER);
        pane.add(this.infoPanel, BorderLayout.SOUTH);
    }

    private class GameBoardPanel extends JPanel implements Updater {
        private final int rows;
        private final int columns;
        private final int totalCells;

        private final JLabel[] cells;
        private final IconManager iconManager;

        GameBoardPanel() throws IOException {
            this.rows = gameBoard.getRowsCount();
            this.columns = gameBoard.getColumnsCount();
            this.totalCells = this.rows * this.columns;
            this.cells = new JLabel[this.totalCells];
            this.iconManager = new IconManager();

            this.init();
        }

        private class IconManager {
            private final ImageIcon[] icons = new ImageIcon[TOTAL_ICONS];
            private final ImageIcon[] resizedIcons = new ImageIcon[TOTAL_ICONS];

            private static final int TOTAL_ICONS = 14;

            private static final int DRAW_MINE = 9;
            private static final int DRAW_COVER = 10;
            private static final int DRAW_MARK = 11;
            private static final int DRAW_WRONG_MARK = 12;
            private static final int DRAW_ALT_COVER = 13;

            IconManager() throws IOException {
                this.loadIcons();
            }

            private void loadIcons() throws IOException {
                for (int i = 0; i < TOTAL_ICONS; i++) {

                    var path = String.format("/png/%d.png", i);
                    var data = this.getClass().getResource(path).openStream();
                    var bytes = data.readAllBytes();
                    this.resizedIcons[i] = this.icons[i] = new ImageIcon(bytes);
                }
            }

            void resizeIcons(
                    int width,
                    int height) {
                for (int i = 0; i < TOTAL_ICONS; ++i) {
                    this.resizedIcons[i] = new ImageIcon(
                            this.icons[i].getImage().getScaledInstance(
                                    width,
                                    height,
                                    Image.SCALE_FAST
                            )
                    );
                }
            }

            ImageIcon getIcon(
                    int offset,
                    int cellValue,
                    boolean inGame) {
                int iconIndex = cellValue;

                if (inGame) {
                    if (CELL_STATE.isMarked(cellValue)) {
                        iconIndex = DRAW_MARK;
                    } else if (CELL_STATE.isCovered(cellValue)) {
                        iconIndex = (offset % GameBoardPanel.this.rows
                                + offset / GameBoardPanel.this.columns) % 2 == 0 ? DRAW_COVER : DRAW_ALT_COVER;
                    }
                } else {
                    if (CELL_STATE.isMine(cellValue) && !CELL_STATE.isMarked(cellValue)) {
                        iconIndex = DRAW_MINE;
                    } else if (CELL_STATE.isMine(cellValue) && CELL_STATE.isMarked(cellValue)) {
                        iconIndex = DRAW_MARK;
                    } else if (!CELL_STATE.isMine(cellValue) && CELL_STATE.isMarked(cellValue)) {
                        iconIndex = DRAW_WRONG_MARK;
                    } else if (CELL_STATE.isCovered(cellValue)) {
                        iconIndex = (offset % GameBoardPanel.this.rows
                                + offset / GameBoardPanel.this.columns) % 2 == 0 ? DRAW_COVER : DRAW_ALT_COVER;
                    }
                }

                return this.resizedIcons[iconIndex];
            }
        }

        private void init() {
            this.setLayout(new GridLayout(this.rows, this.columns));

            for (int i = 0; i < this.totalCells; ++i) {
                this.cells[i] = new JLabel();
                this.cells[i].setIcon(this.iconManager.getIcon(i, 0, true));
                int finalI = i;
                this.cells[i].addMouseListener(
                        new MouseAdapter() {

                            @Override
                            public void mousePressed(MouseEvent e) {
                                GameWindow.this.gameBoard.updateCell(
                                        finalI % GameBoardPanel.this.columns,
                                        finalI / GameBoardPanel.this.columns,
                                        e.getButton() == MouseEvent.BUTTON1
                                );
                            }
                        }
                );

                this.add(this.cells[i]);
            }

            this.addComponentListener(
                    new ComponentAdapter() {

                        @Override
                        public void componentResized(ComponentEvent e) {
                            int width = GameBoardPanel.this.getWidth() / GameBoardPanel.this.columns;
                            int height = GameBoardPanel.this.getHeight() / GameBoardPanel.this.rows;
                            GameBoardPanel.this.iconManager.resizeIcons(
                                    width,
                                    height
                            );
                            GameBoardPanel.this.update(UPDATE_TYPE.VISUALS);
                        }
                    });
        }

        @Override
        public void update(UPDATE_TYPE type) {
            switch (type) {
                case VISUALS:
                case TIMED:
                case ALL:
                    for (int i = 0; i < this.totalCells; ++i) {
                        var icon = this.iconManager.getIcon(
                                i,
                                GameWindow.this.gameBoard.getCellValue(
                                        i % this.columns,
                                        i / this.columns
                                ),
                                GameWindow.this.gameBoard.isInGame()
                        );
                        this.cells[i].setIcon(icon);
                    }
            }
        }
    }

    private class InfoPanel extends JPanel implements Updater {
        private final JLabel status = new JLabel();
        private final JLabel elapsedTime = new JLabel("nan");

        InfoPanel() {
            this.init();
        }

        private void init() {
            this.setLayout(new BorderLayout());
            this.add(this.status, BorderLayout.WEST);
            this.add(this.elapsedTime, BorderLayout.EAST);
        }

        @Override
        public void update(UPDATE_TYPE type) {
            switch (type) {
                case ALL:
                    this.update(UPDATE_TYPE.TIMED);
                    this.update(UPDATE_TYPE.VISUALS);
                    break;
                case TIMED:
                    this.elapsedTime.setText(LocalTime.ofSecondOfDay(
                            GameWindow.this.gameBoard.getElapsedSeconds())
                                                     .format(DateTimeFormatter.ofPattern("HH:mm:ss "))
                    );
                    break;
                case VISUALS:
                    this.status.setText(GameWindow.this.gameBoard.getStatus());
                    break;
            }
        }
    }

    @Override
    public void update(UPDATE_TYPE type) {
        switch (type) {
            case ALL:
                this.update(UPDATE_TYPE.TIMED);
                this.update(UPDATE_TYPE.VISUALS);
                break;
            case TIMED:
                this.infoPanel.update(UPDATE_TYPE.TIMED);
                break;
            case VISUALS:
                this.infoPanel.update(UPDATE_TYPE.VISUALS);
                this.gameBoardPanel.update(UPDATE_TYPE.VISUALS);
                break;
        }
    }

    @Override
    public void run() {
        this.gameBoard.newGame();
        SwingUtilities.invokeLater(() -> this.setVisible(true));
    }
}
