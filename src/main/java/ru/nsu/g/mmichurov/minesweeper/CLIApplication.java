package ru.nsu.g.mmichurov.minesweeper;

import ru.nsu.g.mmichurov.minesweeper.view.cli.MainMenuView;

public class CLIApplication {
    public static void main(String[] args) {
        new MainMenuView().run();
    }
}
