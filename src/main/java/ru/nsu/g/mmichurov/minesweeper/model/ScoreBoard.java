package ru.nsu.g.mmichurov.minesweeper.model;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ScoreBoard {
    private static String fileName = "scores.txt";

    public static ArrayList<Entry> getScoresList() {
        Scanner inputScanner;
        var result = new ArrayList<Entry>();

        try {
            inputScanner = new Scanner(new FileInputStream(ScoreBoard.fileName));
        } catch (FileNotFoundException e) {
            return result;
        }

        while (inputScanner.hasNextLine()) {
            result.add(Entry.fromString(inputScanner.nextLine()));
        }

        result.sort((e1, e2) -> {
            int probabilityDifference = (int) (e1.rowsCount * e1.columnsCount * 100.0 / e1.minesCount - e2.rowsCount * e2.columnsCount * 100.0 / e2.minesCount);

            if (probabilityDifference == 0) {
                return e1.timeTaken - e2.timeTaken;
            }

            return probabilityDifference;
        });

        return result;
    }

    public static void addScoreEntry(Entry newScoreEntry) throws IOException {
        var outputStream = new PrintStream(new FileOutputStream(ScoreBoard.fileName, true));
        outputStream.println(newScoreEntry.toString());
    }

    public static class Entry {
        public final String playerName;
        public final int rowsCount;
        public final int columnsCount;
        public final int minesCount;
        public final int timeTaken;

        public Entry(
                String playerName,
                int rowsCount,
                int columnsCount,
                int minesCount,
                int timeTaken) {
            this.playerName = playerName.isBlank() ? "Unknown" : playerName;
            this.rowsCount = rowsCount;
            this.columnsCount = columnsCount;
            this.minesCount = minesCount;
            this.timeTaken = timeTaken;
        }

        @Override
        public String toString() {
            return String.format(
                    "%s %d %d %d %d",
                    this.playerName,
                    this.rowsCount,
                    this.columnsCount,
                    this.minesCount,
                    this.timeTaken
            );
        }

        public static Entry fromString(String stringRepr) {
            var tokenizer = new Scanner(stringRepr);

            String playerName = tokenizer.hasNext() ? tokenizer.next() : "";
            int rowsCount = tokenizer.hasNextInt() ? tokenizer.nextInt() : -1;
            int columnsCount = tokenizer.hasNextInt() ? tokenizer.nextInt() : -1;
            int minesCount = tokenizer.hasNextInt() ? tokenizer.nextInt() : -1;
            int timeTaken = tokenizer.hasNextInt() ? tokenizer.nextInt() : -1;

            return new Entry(playerName, rowsCount, columnsCount, minesCount, timeTaken);
        }
    }
}
