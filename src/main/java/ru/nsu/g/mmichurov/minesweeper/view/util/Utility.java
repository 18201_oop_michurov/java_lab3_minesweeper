package ru.nsu.g.mmichurov.minesweeper.view.util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

public class Utility {
    public static void resizeComponentBasedOnOther(JComponent component, Dimension baseDimension) {
        Utility.resizeComponentBasedOnOther(component, baseDimension, 2.5, 3.5);
    }

    public static void resizeComponentBasedOnOther(
            JComponent component,
            Dimension baseDimension,
            double widthMultiplier,
            double heightMultiplier) {
        component.setPreferredSize(
                new Dimension(
                        (int) (baseDimension.getWidth() * widthMultiplier),
                        (int) (baseDimension.getHeight() * heightMultiplier)
                )
        );
    }

    public static void showFrame(Runnable newFrame) {
        javax.swing.SwingUtilities.invokeLater(newFrame);
    }

    public static void closeFrame(JFrame toBeClosed) {
        toBeClosed.dispatchEvent(new WindowEvent(toBeClosed, WindowEvent.WINDOW_CLOSING));
    }

    public static void switchFrames(JFrame toBeDisposed, Runnable newFrame) {
        javax.swing.SwingUtilities.invokeLater(newFrame);
        toBeDisposed.dispose();
    }
}
