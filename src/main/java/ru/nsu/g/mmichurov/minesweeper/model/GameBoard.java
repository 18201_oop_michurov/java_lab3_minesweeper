package ru.nsu.g.mmichurov.minesweeper.model;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
import java.util.concurrent.ThreadLocalRandom;

public class GameBoard {
    private final String playerName;
    private final int rows;
    private final int columns;
    private final int totalMines;
    private int minesLeft;
    private int uncovered;
    private boolean inGame = false;
    private String status;

    private int[] board;

    private final CellUpdater cellUpdater = new CellUpdater();
    final BoardConstructor boardConstructor = new BoardConstructor();
    private final Timer timer;
    private int elapsedSeconds;
    private Updater view;

    public GameBoard(
            String playerName,
            int rows,
            int columns,
            int totalMines)
    {
        this.playerName = playerName;
        this.rows = rows <= 0 ? 9 : rows;
        this.columns = columns <= 0 ? 9 : columns;
        this.totalMines = Math.max(1, Math.min(totalMines, rows * columns));
        this.timer = new Timer(1000, e -> {
            this.elapsedSeconds += 1;
            this.view.update(Updater.UPDATE_TYPE.TIMED);
        });
    }

    public void newGame() {
        this.inGame = true;
        this.boardConstructor.generateBoard();
        this.minesLeft = totalMines;
        this.uncovered = 0;
        this.status = String.valueOf(this.totalMines);
        this.elapsedSeconds = 0;
        this.timer.start();
        this.view.update(Updater.UPDATE_TYPE.ALL);
    }

    public static class CELL_STATE {
        static final int NON_EMPTY   = 0b00001111;
        static final int MINE        = 0b00010000;
        static final int COVERED     = 0b00100000;
        static final int MARKED      = 0b01000000;

        public static boolean isMine(int cellValue) {
            return check(cellValue, MINE);
        }

        public static boolean isEmpty(int cellValue) {
            return !check(cellValue, NON_EMPTY);
        }

        public static boolean isCovered(int cellValue) {
            return check(cellValue, COVERED);
        }

        public static boolean isMarked(int cellValue) {
            return check(cellValue, MARKED);
        }

        public static int getNeighbouringMinesCount(int cellValue) {
            return cellValue & NON_EMPTY;
        }

        private static boolean check(int value, int mask) {
            return (value & mask) != 0;
        }
    }

    class BoardConstructor {
        void generateBoard() {
            GameBoard.this.board = new int[GameBoard.this.rows * GameBoard.this.columns];
            int totalCells = GameBoard.this.columns * GameBoard.this.rows;

            for (int i = 0; i < totalCells; ++i) {
                GameBoard.this.board[i] = CELL_STATE.COVERED;
            }

            int placedMines = 0;

            while (placedMines < GameBoard.this.totalMines) {
                int cellOffset = ThreadLocalRandom.current().nextInt(0, totalCells);

                if (!CELL_STATE.isMine(GameBoard.this.board[cellOffset])) {
                    this.placeMine(cellOffset);
                    placedMines += 1;
                }
            }
        }

        void placeMine(int offset) {
            GameBoard.this.board[offset] = CELL_STATE.MINE | CELL_STATE.COVERED;

            for (int i : this.getNeighbours(offset)) {
                if (!CELL_STATE.isMine(GameBoard.this.board[i])) {
                    GameBoard.this.board[i] += 1;
                }
            }
        }

        boolean isValid(int x, int y) {
            return x >= 0 && y >= 0 && x < GameBoard.this.columns && y < GameBoard.this.rows;
        }

        ArrayList<Integer> getNeighbours(int offset) {
            int x = offset % GameBoard.this.columns;
            int y = offset / GameBoard.this.columns;

            var neighbours = new ArrayList<Integer>();

            if (this.isValid(x - 1, y - 1)) {
                neighbours.add((y - 1) * GameBoard.this.columns + x - 1);
            }
            if (this.isValid(x, y - 1)) {
                neighbours.add((y - 1) * GameBoard.this.columns + x);
            }
            if (this.isValid(x + 1, y - 1)) {
                neighbours.add((y - 1) * GameBoard.this.columns + x + 1);
            }
            if (this.isValid(x - 1, y)) {
                neighbours.add(y * GameBoard.this.columns + x - 1);
            }
            if (this.isValid(x + 1, y)) {
                neighbours.add(y * GameBoard.this.columns + x + 1);
            }
            if (this.isValid(x - 1, y + 1)) {
                neighbours.add((y + 1) * GameBoard.this.columns + x - 1);
            }
            if (this.isValid(x, y + 1)) {
                neighbours.add((y + 1) * GameBoard.this.columns + x);
            }
            if (this.isValid(x + 1, y + 1)) {
                neighbours.add((y + 1) * GameBoard.this.columns + x + 1);
            }

            return neighbours;
        }
    }

    private class CellUpdater {
        private boolean shouldUpdateView;

        private boolean shouldUpdateMineCount;

        private void uncoverEmptyNeighbours(int offset) {
            var neighboursStack = new Stack<Integer>();
            neighboursStack.addAll(GameBoard.this.boardConstructor.getNeighbours(offset));

            do {
                var current = neighboursStack.pop();

                if (CELL_STATE.isCovered(GameBoard.this.board[current])) {
                    GameBoard.this.board[current] &= ~CELL_STATE.COVERED;
                    GameBoard.this.uncovered += 1;

                    if (CELL_STATE.isMarked(GameBoard.this.board[current])) {
                        GameBoard.this.board[current] &= ~CELL_STATE.MARKED;
                        GameBoard.this.minesLeft += 1;
                        GameBoard.this.uncovered -= 1;

                        this.shouldUpdateMineCount = true;
                    }

                    if (CELL_STATE.isEmpty(GameBoard.this.board[current])) {
                        neighboursStack.addAll(GameBoard.this.boardConstructor.getNeighbours(current));
                    }
                }
            } while (!neighboursStack.isEmpty());
        }

        private void updateOnLeftMouseClick(int offset) {
            int cellValue = GameBoard.this.board[offset];

            if (CELL_STATE.isMarked(cellValue)) {
                return;
            }

            if (CELL_STATE.isCovered(cellValue)) {
                board[offset] &= ~CELL_STATE.COVERED;
                GameBoard.this.uncovered += 1;
                this.shouldUpdateView = true;

                if (CELL_STATE.isMine(cellValue)) {
                    GameBoard.this.uncovered -= 1;
                    GameBoard.this.inGame = false;
                    GameBoard.this.status = "Game Lost";
                } else if (CELL_STATE.isEmpty(cellValue)) {
                    this.uncoverEmptyNeighbours(offset);
                }
            }
        }

        private void updateOnOtherClick(int offset) {
            int cellValue = GameBoard.this.board[offset];

            if (CELL_STATE.isCovered(cellValue)) {
                this.shouldUpdateView = true;

                if (CELL_STATE.isMarked(cellValue)) {
                    GameBoard.this.board[offset] &= ~CELL_STATE.MARKED;

                    GameBoard.this.minesLeft += 1;
                    GameBoard.this.uncovered -= 1;

                    this.shouldUpdateMineCount = true;
                } else {
                    if (GameBoard.this.minesLeft > 0) {
                        GameBoard.this.board[offset] |= CELL_STATE.MARKED;

                        GameBoard.this.minesLeft -= 1;
                        GameBoard.this.uncovered += 1;

                        this.shouldUpdateMineCount = true;
                    } else {
                        GameBoard.this.status = "No marks left";
                    }
                }
            }
        }

        void updateCell(int offset, boolean isLMB) {
            this.shouldUpdateView = false;
            this.shouldUpdateMineCount = false;

            if (isLMB) {
                this.updateOnLeftMouseClick(offset);
            } else {
                this.updateOnOtherClick(offset);
            }


            if (this.shouldUpdateMineCount) {
                GameBoard.this.status = String.valueOf(GameBoard.this.minesLeft);
            }
            if (GameBoard.this.uncovered == GameBoard.this.rows * GameBoard.this.columns) {
                GameBoard.this.inGame = false;
                GameBoard.this.status = "Game Won!";
                try {
                    ScoreBoard.addScoreEntry(
                            new ScoreBoard.Entry(
                                    GameBoard.this.playerName,
                                    GameBoard.this.rows,
                                    GameBoard.this.columns,
                                    GameBoard.this.totalMines,
                                    GameBoard.this.elapsedSeconds
                            )
                    );
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (!GameBoard.this.inGame) {
                GameBoard.this.timer.stop();
            }
        }

        boolean shouldUpdateView() {
            return this.shouldUpdateView;
        }
    }

    public void updateCell(int x, int y, boolean isLMB) {
        if (!this.inGame) {
            this.newGame();
            this.view.update(Updater.UPDATE_TYPE.ALL);

            return;
        }

        int offset = y * this.columns + x;

        this.cellUpdater.updateCell(offset, isLMB);

        if (this.cellUpdater.shouldUpdateView()) {
            this.view.update(Updater.UPDATE_TYPE.VISUALS);
        }
    }

    public int getCellValue(int x, int y) throws ArrayIndexOutOfBoundsException {
        try {
            return this.board[y * this.columns + x];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException(
                    String.format(
                            "Position (%d, %d) is outside board bounds (%d x %d)",
                            x, y,
                            this.columns, this.rows
                    )
            );
        }
    }

    public int getRowsCount() {
        return this.rows;
    }

    public int getColumnsCount() {
        return this.columns;
    }

    int getMinesCount() {
        return this.totalMines;
    }

    public String getStatus() {
        return this.status;
    }

    public int getElapsedSeconds() {
        return this.elapsedSeconds;
    }

    int[] getRawBoard() {
        return this.board;
    }

    public boolean isInGame() {
        return this.inGame;
    }

    public void setView(Updater view) {
        this.view = view;
    }
}
