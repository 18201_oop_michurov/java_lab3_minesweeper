package ru.nsu.g.mmichurov.minesweeper.view.gui;

import ru.nsu.g.mmichurov.minesweeper.model.GameBoard;
import ru.nsu.g.mmichurov.minesweeper.model.ScoreBoard;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class MainMenuWindow extends JFrame implements Runnable {

    public MainMenuWindow() {
        super("Minesweeper");

        this.addComponentsToPane(this.getContentPane());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.pack();
        this.setLocationRelativeTo(null);
    }

    private class NewGameTab extends JPanel {
        NewGameTab() {
            this.init();
        }

        private void init() {
            var settings = new JPanel();
            settings.setLayout(new GridLayout(4, 2));

            var playerNameTextBox = new JTextField();
            var rowsSelector = new JSpinner(new SpinnerNumberModel(9, 4, 30, 1));
            var columnsSelector = new JSpinner(new SpinnerNumberModel(9, 4, 30, 1));
            var minesCountSelector = new JSpinner(new SpinnerNumberModel(10, 1, 300, 1));
            var startButton = new JButton("Start");

            startButton.addActionListener(actionEvent -> {
                int rowsCount = (int) rowsSelector.getValue();
                int columnsCount = (int) columnsSelector.getValue();
                int minesCount = (int) minesCountSelector.getValue();
                String playerName = playerNameTextBox.getText();
                try {
                    var gameBoard = new GameBoard(playerName, rowsCount, columnsCount, minesCount);
                    var gameWindow = new GameWindow(gameBoard);
                    gameWindow.run();
                    MainMenuWindow.this.dispose();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            settings.add(new JLabel("Player Name:"));
            settings.add(playerNameTextBox);
            settings.add(new JLabel("Rows Count:"));
            settings.add(rowsSelector);
            settings.add(new JLabel("Columns Count:"));
            settings.add(columnsSelector);
            settings.add(new JLabel("Mines Count:"));
            settings.add(minesCountSelector);

            this.add(settings);
            this.add(startButton);
        }
    }

    private static class AboutTab extends JPanel {
        AboutTab() {
            this.init();
        }

        private void init() {
            var text = "<html>" +
                    "Minesweeper is a single-player puzzle video game.<br>" +
                    "The objective of the game is to clear a rectangular<br>" +
                    "board containing hidden \"mines\" or bombs without<br>" +
                    "detonating any of them, with help from clues about<br>" +
                    "the number of neighboring mines in each field.";
            this.add(new JLabel(text));
        }
    }

    private static class HighScoresTab extends JPanel {
        HighScoresTab() {
            this.init();
        }

        private void init() {
            this.setLayout(new BorderLayout());
            var infoPanel = new JPanel(new GridLayout(1, 4));
            infoPanel.add(new JLabel("Name"));
            infoPanel.add(new JLabel("Board Size"));
            infoPanel.add(new JLabel("Mines Count"));
            infoPanel.add(new JLabel("Time Taken   "));
            this.add(infoPanel, BorderLayout.NORTH);

            JPanel mainList = new JPanel();
            mainList.setLayout(new BoxLayout(mainList, BoxLayout.Y_AXIS));

            this.add(new JScrollPane(
                    mainList,
                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                    JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
            ));

            var scores = ScoreBoard.getScoresList();

            for (var e : scores) {
                JPanel panel = new JPanel(new GridLayout(1, 4));
                panel.add(new JLabel(e.playerName));
                panel.add(new JLabel(String.format("%d x %d", e.rowsCount, e.columnsCount)));
                panel.add(new JLabel(String.valueOf(e.minesCount), SwingConstants.CENTER));
                panel.add(new JLabel(
                        LocalTime.ofSecondOfDay(e.timeTaken).format(DateTimeFormatter.ofPattern("HH:mm:ss")),
                        SwingConstants.CENTER
                ));
                mainList.add(panel);
            }
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(this.getWidth(), 150);
        }
    }

    private void addComponentsToPane(final Container pane) {
        var mainPane = new JTabbedPane();

        mainPane.addTab("New Game", new NewGameTab());
        mainPane.addTab("High Scores", new HighScoresTab());
        mainPane.addTab("About", new AboutTab());

        pane.add(mainPane);
    }

    @Override
    public void run() {
        SwingUtilities.invokeLater(() -> this.setVisible(true));
    }
}
