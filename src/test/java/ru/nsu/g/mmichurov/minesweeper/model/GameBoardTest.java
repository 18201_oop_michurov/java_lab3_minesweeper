package ru.nsu.g.mmichurov.minesweeper.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameBoardTest {
    private static final int ROWS = 10;
    private static final int COLUMNS = 10;
    private static final int MINES = 35;

    @Test
    void construct() {
        var gameBoard = new GameBoard("", 0, 0, 0);

        assertEquals(9, gameBoard.getRowsCount());
        assertEquals(9, gameBoard.getColumnsCount());
        assertEquals(1, gameBoard.getMinesCount());

        gameBoard = new GameBoard("", 10, 10, 1000);

        assertEquals(10, gameBoard.getRowsCount());
        assertEquals(10, gameBoard.getColumnsCount());
        assertEquals(100, gameBoard.getMinesCount());

        gameBoard = new GameBoard("", ROWS, COLUMNS, MINES);

        assertEquals(ROWS, gameBoard.getRowsCount());
        assertEquals(COLUMNS, gameBoard.getColumnsCount());
        assertEquals(MINES, gameBoard.getMinesCount());
    }

    @Test
    void newGame() {
        var gameBoard = new GameBoard("", ROWS, COLUMNS, MINES);

        gameBoard.setView(type -> {});
        gameBoard.newGame();

        int actualMines = 0;

        for (int y = 0; y < gameBoard.getRowsCount(); ++y) {
            for (int x = 0; x < gameBoard.getColumnsCount(); ++x) {
                int cellValue = gameBoard.getCellValue(x, y);
                if (GameBoard.CELL_STATE.isMine(cellValue)) {
                    actualMines += 1;
                } else {
                    int neighbouringMinesCount = GameBoard.CELL_STATE.getNeighbouringMinesCount(cellValue);
                    int actual = 0;

                    var neighbours = gameBoard.boardConstructor.getNeighbours(
                            y * gameBoard.getColumnsCount() + x
                    );
                    for (var offset : neighbours) {
                        if (GameBoard.CELL_STATE.isMine(
                                gameBoard.getCellValue(
                                        offset % gameBoard.getColumnsCount(),
                                        offset / gameBoard.getColumnsCount()
                                ))) {
                            actual += 1;
                        }
                    }

                    assertEquals(neighbouringMinesCount, actual);
                }
            }
        }

        assertEquals(MINES, actualMines);
    }

    @Test
    void updateCellWin() {
        var gameBoard = new GameBoard("", ROWS, COLUMNS, MINES);

        gameBoard.setView(type -> {});
        gameBoard.newGame();

        int minesLeft = MINES;

        for (int y = 0; y < gameBoard.getRowsCount(); ++y) {
            for (int x = 0; x < gameBoard.getColumnsCount(); ++x) {
                int cellValue = gameBoard.getCellValue(x, y);

                if (GameBoard.CELL_STATE.isMine(cellValue)) {
                    gameBoard.updateCell(x, y, false);
                    minesLeft -= 1;

                    if (minesLeft > 0) {
                        assertEquals(String.valueOf(minesLeft), gameBoard.getStatus());
                    }
                } else {
                    gameBoard.updateCell(x, y, true);
                }
            }
        }

        assertFalse(gameBoard.isInGame());
        assertEquals("Game Won!", gameBoard.getStatus());
    }

    @Test
    void updateCellLoss() {
        var gameBoard = new GameBoard("", ROWS, COLUMNS, MINES);

        gameBoard.setView(type -> {});
        gameBoard.newGame();

        outer:
        for (int y = 0; y < gameBoard.getRowsCount(); ++y) {
            for (int x = 0; x < gameBoard.getColumnsCount(); ++x) {
                int cellValue = gameBoard.getCellValue(x, y);

                if (GameBoard.CELL_STATE.isMine(cellValue)) {
                    gameBoard.updateCell(x, y, true);
                    break outer;
                }
            }
        }

        assertFalse(gameBoard.isInGame());
        assertEquals("Game Lost", gameBoard.getStatus());
    }

    @Test
    void getCellValue() {
        var gameBoard = new GameBoard("", ROWS, COLUMNS, MINES);

        gameBoard.setView(type -> {});
        gameBoard.newGame();

        var rawBoard = gameBoard.getRawBoard();

        for (int y = 0; y < gameBoard.getRowsCount(); ++y) {
            for (int x = 0; x < gameBoard.getColumnsCount(); ++x) {
                assertEquals(rawBoard[y * gameBoard.getColumnsCount() + x], gameBoard.getCellValue(x, y));
            }
        }
    }
}