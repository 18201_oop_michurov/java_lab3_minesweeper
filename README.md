## Build

```bash
$ ./gardlew jar
```
## Run

CLI
```bash
$ java -jar build/libs/minesweeper-cli.jar
```
GUI
```bash
$ java -jar build/libs/minesweeper-gui.jar
```
